from threading import Event
class CondVar:
    def __init__(self, cond_var_name):
        self.cond_var_name = cond_var_name
        self.cond_var = Event()
        self.cond_var.clear()
        self.host_map = {}

    def wait(self):
        self.cond_var.wait()
        self.cond_var.clear()

    def signal(self):
        self.cond_var.set()
