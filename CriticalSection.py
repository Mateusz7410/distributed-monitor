class CriticalSection:
    def __init__(self, name, my_ip_addr):
        self.name = name
        self.is_locked = False
        self.is_trying_to_lock = False
        self.is_waiting = False
        self.host_map = {}
        self.no_map = {}
        self.my_ip_addr = my_ip_addr

    def get_host(self, cond_var_map):
        min_round_no = -1
        min_host = self.my_ip_addr


        for key, value in self.host_map.items():

            if value.ip_addr == self.my_ip_addr:
                continue

            if value.ip_addr in self.__check_cond_var_map(cond_var_map):
                continue

            if min_round_no == -1:
                min_round_no = value.round_no
                min_host = value.ip_addr

            if value.round_no < min_round_no:
                min_round_no = value.round_no
                min_host = value.ip_addr


            if (value.round_no == min_round_no) and (int(value.ip_addr.split(".")[-1]) < int(self.my_ip_addr.split(".")[-1])):
                min_host = value.ip_addr
        return min_host

    def __check_cond_var_map(self, cond_var_map):
        res = []
        for key, value in cond_var_map.items():
            for k, v in value.host_map.items():
                if v == self.name:
                    res.append(k)
        return res
