class Message:
    def __init__(self, type, round_no, ip_addr, crit_sec_name, want_to_lock):
        self.type = type
        self.round_no = int(round_no)
        self.ip_addr = ip_addr
        self.crit_sec_name = crit_sec_name
        self.want_to_lock = want_to_lock
