from Monitor import Monitor
import time

buffer_size = 1

monitor = Monitor("192.168.0.19", "4537", 3)
monitor.init(["section1"])
monitor.create_shared_object("buffer", buffer_size)
obj_len = monitor.object_length("buffer")

while True:
    monitor.lock("section1")
    print("Critical Section 'section1' is locked!")
    if obj_len == 0:
        print("Wait for 'reader' signal!")
        monitor.wait("reader")
    object = monitor.read("buffer")
    print("Readed object: " + object)
    if obj_len == buffer_size - 1:
        monitor.signal("writer")
        print("Send signal to writer!")
    #time.sleep(1)

    monitor.unlock("section1")
    print("Critical Section 'section1' is unlocked!\n")

monitor.delete()
