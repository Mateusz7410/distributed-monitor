from Monitor import Monitor
import time

monitor = Monitor("192.168.0.19", "4537", 2)
monitor.init(["section1", "section2"])
monitor.lock("section1")
print("################## W SEKCJI KRYTYCZNEJ ######################")
monitor.signal("cond1")
print("################## WYSLANO SYGNAL ######################")
time.sleep(1)
monitor.unlock("section1")
print("################## POZA SEKCJA KRYTYCZNEJ ######################")
monitor.delete()
