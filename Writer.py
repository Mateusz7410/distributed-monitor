from Monitor import Monitor
import time

buffer_size = 1

monitor = Monitor("192.168.0.19", "4537", 3)
monitor.init(["section1"])
monitor.create_shared_object("buffer", buffer_size)
obj_len = monitor.object_length("buffer")

inc = 0
while True:
    monitor.lock("section1")
    print("Critical Section 'section1' is locked!")
    if obj_len == buffer_size:
        print("Wait for 'writer' signal!")
        monitor.wait("writer")
    monitor.write("buffer", "testObject" + str(inc))
    print("testObject" + str(inc) + " added to 'buffer' object!")
    if obj_len == 1:
        monitor.signal("reader")
        print("Send signal to reader!")
    #time.sleep(1)

    monitor.unlock("section1")
    print("Critical Section 'section1' is unlocked!\n")
    inc += 1

monitor.delete()
