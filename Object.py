class Object:
    def __init__(self, object_name, object_len):
        self.object_name = object_name
        self.object_len = object_len
        self.actual_object_len = Mutable(0)
        self.object_list = []

    def add(self, object):
        if len(self.object_list) < self.object_len:
            self.object_list.append(object)
            self.actual_object_len.val = len(self.object_list)
        else:
            raise Exception('Monitor tried to write to empty object!!!')

    def remove(self):
        if len(self.object_list) > 0:
            ob = self.object_list.pop(0)
            self.actual_object_len.val = len(self.object_list)
            return ob
        else:
            raise Exception('Monitor tried to read from empty object!!!')

    def length(self):
        return self.actual_object_len

class Mutable:
    def __init__(self, val):
        self.val = val
    def __lt__(self, other):
        return self.val < other
    def __gt__(self, other):
        return self.val > other
    def __le__(self, other):
        return self.val <= other
    def __ge__(self, other):
        return self.val >= other
    def __eq__(self, other):
        return self.val == other
    def __ne__(self, other):
        return self.val != other
