import zmq
import time
import sys
import socket
from threading import Thread, Event
from FirstConnection import FirstConnection
from CriticalSection import CriticalSection
from Message import Message
from CondVar import CondVar
from Object import Object
import time
import random

class Monitor:
    def __init__(self, dst_ip_addr, port, client_count):
        print("Initializing connection and monitor...")
        fc = FirstConnection(dst_ip_addr, port, client_count)
        self.__quit = False
        self.__my_ip_addr = fc.get_my_ip_addr()
        self.__port = port
        self.__all_ip_addr = fc.get_all_ip_addr()
        self.__token = fc.get_token()
        self.__all_ip_addr.remove(self.__my_ip_addr)
        self.__all_dead_ip_addr = []
        self.__round_no = 0
        self.__context = zmq.Context()
        self.__sockets = {}
        self.__crit_sec_map = {}
        self.__actual_crit_sec = ""
        self.__lock_notify = Event()
        self.__lock_notify.clear()
        self.__is_trying_to_reenter = False
        self.__cond_var_map = {}
        self.__object_map = {}
        self.__waiting_for = ""
        self.__signal_one = ""
        self.__can_send_token = False
        self.create_sockets()

    def delete(self):
        self.__notify_all_about_finishing()
        self.__quit = True
        self.__thread.join()

    def __handler(self):
        socket = self.__context.socket(zmq.REP)
        socket.bind("tcp://*:%s" % self.__port)

        answer = ""
        while(not self.__quit):
            try:
                info = socket.recv(flags=zmq.NOBLOCK)
                msg = self.__get_info(info)
                if msg.type == "lock":
                    cs = self.__crit_sec_map[msg.crit_sec_name]
                    cs.host_map[msg.ip_addr] = msg
                    if self.__can_send_token:
                        socket.send(self.__get_message_format("lock", msg.crit_sec_name, "token"))
                        self.__token = False
                        self.__can_send_token = False
                    else:
                        socket.send(self.__get_message_format("lock", msg.crit_sec_name, "yes"))
                elif msg.type == "unlock":
                    cs = self.__crit_sec_map[msg.crit_sec_name]
                    if msg.ip_addr in cs.host_map:
                        cs.host_map.pop(msg.ip_addr)
                    socket.send(self.__get_message_format("unlock", msg.crit_sec_name, "yes"))
                elif msg.type == "token":
                    self.__token = True
                    self.__lock_notify.set()
                    socket.send(self.__get_message_format("token", msg.crit_sec_name, "yes"))
                elif msg.type == "finish":
                    self.__all_dead_ip_addr.append(msg.ip_addr)
                    socket.send(self.__get_message_format("finish", msg.crit_sec_name, "yes"))
                elif msg.type == "wait":
                    [msg.crit_sec_name, cond_var_name] = msg.crit_sec_name.split("^")
                    if(cond_var_name not in self.__cond_var_map):
                        self.__cond_var_map[cond_var_name] = CondVar(cond_var_name)
                    self.__cond_var_map[cond_var_name].host_map[msg.ip_addr] = msg.crit_sec_name
                    socket.send(self.__get_message_format("wait", cond_var_name, "yes"))
                elif msg.type == "signal_all":
                    [cond_var_name, choosen_ip_addr] = msg.crit_sec_name.split("^")
                    if(cond_var_name in self.__cond_var_map):
                        if choosen_ip_addr in self.__cond_var_map[cond_var_name].host_map:
                            self.__cond_var_map[cond_var_name].host_map.pop(choosen_ip_addr)
                    socket.send(self.__get_message_format("signal_all", cond_var_name, "yes"))
                elif msg.type == "signal_one":
                    [cond_var_name, choosen_ip_addr] = msg.crit_sec_name.split("^")
                    socket.send(self.__get_message_format("signal_one", cond_var_name, "yes"))
                    self.__cond_var_map[cond_var_name].signal()
                elif msg.type == "write":
                    [object_name, object] = msg.crit_sec_name.split("^")
                    self.__object_map[object_name].add(object)
                    socket.send(self.__get_message_format("write", object_name, "yes"))
                elif msg.type == "read":
                    object_name = msg.crit_sec_name
                    self.__object_map[object_name].remove()
                    socket.send(self.__get_message_format("read", object_name, "yes"))
            except zmq.ZMQError, e:
                if e.errno == zmq.EAGAIN:
                    pass
                else:
                    raise


    def __get_message_format(self, type, crit_sec_name, want_to_lock):
        return str(type + ";" + str(self.__round_no) + ";" + self.__my_ip_addr + ";" + crit_sec_name + ";" + want_to_lock)

    def __get_info(self, msg):
        info_list = msg.split(";")
        return Message(info_list[0], info_list[1], info_list[2], info_list[3], info_list[4])

    def __notify_all_about_locking(self, crit_sec_name):
        self.__can_send_token = False
        if not self.__token:
            msg = self.__get_message_format("lock", crit_sec_name, "yes")
            token = False
            for ip_addr in self.__all_ip_addr:
                if ip_addr in self.__all_dead_ip_addr:
                    continue

                socket = self.__sockets[ip_addr]

                socket.send(msg)
                while (ip_addr not in self.__all_dead_ip_addr):
                    try:
                        info = socket.recv(flags=zmq.NOBLOCK)
                        answer = self.__get_info(info)
                        if answer.want_to_lock == "token":
                            token = True
                        break
                    except zmq.ZMQError, e:
                        if e.errno == zmq.EAGAIN:
                            pass
                        else:
                            raise
            if not token:
                self.__lock_notify.wait()
                self.__lock_notify.clear()
            self.__token = True
        self.__round_no += 1

    def __notify_all_about_unlocking(self, crit_sec_name):
        msg = self.__get_message_format("unlock", crit_sec_name, "yes")
        for ip_addr in self.__all_ip_addr:
            if ip_addr in self.__all_dead_ip_addr:
                continue

            socket = self.__sockets[ip_addr]

            socket.send(msg)
            while (ip_addr not in self.__all_dead_ip_addr):
                try:
                    socket.recv(flags=zmq.NOBLOCK)
                    break
                except zmq.ZMQError, e:
                    if e.errno == zmq.EAGAIN:
                        pass
                    else:
                        raise

        if self.__signal_one != "":
            [cond_var_name, choosen_ip_addr] = self.__signal_one.split("^")
            socket = self.__sockets[choosen_ip_addr]
            msg = self.__get_message_format("signal_one", self.__signal_one, "yes")
            socket.send(msg)
            while (choosen_ip_addr not in self.__all_dead_ip_addr):
                try:
                    socket.recv(flags=zmq.NOBLOCK)
                    break
                except zmq.ZMQError, e:
                    if e.errno == zmq.EAGAIN:
                        pass
                    else:
                        raise
            self.__signal_one = ""
            self.__token = False
        else:
            cs = self.__crit_sec_map[crit_sec_name]

            host = cs.get_host(self.__cond_var_map)
            if host != self.__my_ip_addr:
                socket = self.__sockets[host]

                msg = self.__get_message_format("token", crit_sec_name, "yes")

                socket.send(msg)
                while (ip_addr not in self.__all_dead_ip_addr):
                    try:
                        socket.recv(flags=zmq.NOBLOCK)
                        break
                    except zmq.ZMQError, e:
                        if e.errno == zmq.EAGAIN:
                            pass
                        else:
                            raise
                self.__token = False
            else:
                self.__can_send_token = True

    def __notify_all_about_finishing(self):
        msg = self.__get_message_format("finish", "none", "no")

        for ip_addr in self.__all_ip_addr:
            if ip_addr in self.__all_dead_ip_addr:
                continue

            socket = self.__sockets[ip_addr]

            socket.send(msg)
            while (ip_addr not in self.__all_dead_ip_addr):
                try:
                    socket.recv(flags=zmq.NOBLOCK)
                    break
                except zmq.ZMQError, e:
                    if e.errno == zmq.EAGAIN:
                        pass
                    else:
                        raise

    def __notify_all_about_waiting(self, cond_var_name):
        if self.__actual_crit_sec != "NoCritSection":
            self.__crit_sec_map[self.__actual_crit_sec].is_waiting = True
        msg = self.__get_message_format("wait", self.__actual_crit_sec + "^" + cond_var_name, "yes")

        for ip_addr in self.__all_ip_addr:
            if ip_addr in self.__all_dead_ip_addr:
                continue

            socket = self.__sockets[ip_addr]

            socket.send(msg)
            while (ip_addr not in self.__all_dead_ip_addr):
                try:
                    socket.recv(flags=zmq.NOBLOCK)
                    break
                except zmq.ZMQError, e:
                    if e.errno == zmq.EAGAIN:
                        pass
                    else:
                        raise

        cs = self.__crit_sec_map[self.__actual_crit_sec]

        host = cs.get_host(self.__cond_var_map)
        if host != self.__my_ip_addr:
            socket = self.__sockets[host]

            msg = self.__get_message_format("token", self.__actual_crit_sec, "yes")

            socket.send(msg)
            while (ip_addr not in self.__all_dead_ip_addr):
                try:
                    socket.recv(flags=zmq.NOBLOCK)
                    break
                except zmq.ZMQError, e:
                    if e.errno == zmq.EAGAIN:
                        pass
                    else:
                        raise
        else:
            self.__can_send_token = True


    def __notify_all_about_signal(self, cond_var_name):
        if cond_var_name in self.__cond_var_map:
            cv = self.__cond_var_map[cond_var_name]
            if len(cv.host_map) != 0:
                choosen_ip_addr = random.choice(list(cv.host_map.keys()))
                cv.host_map.pop(choosen_ip_addr)

                msg = self.__get_message_format("signal_all", cond_var_name + "^" + choosen_ip_addr, "yes")

                for ip_addr in self.__all_ip_addr:
                    if choosen_ip_addr == ip_addr:
                        continue

                    if ip_addr in self.__all_dead_ip_addr:
                        continue

                    socket = self.__sockets[ip_addr]

                    socket.send(msg)
                    while (ip_addr not in self.__all_dead_ip_addr):
                        try:
                            socket.recv(flags=zmq.NOBLOCK)
                            break
                        except zmq.ZMQError, e:
                            if e.errno == zmq.EAGAIN:
                                pass
                            else:
                                raise
                self.__signal_one = cond_var_name + "^" + choosen_ip_addr

    def __notify_all_about_write(self, object_name, object):
        msg = self.__get_message_format("write", object_name + "^" + str(object), "yes")

        for ip_addr in self.__all_ip_addr:
            if ip_addr in self.__all_dead_ip_addr:
                continue

            socket = self.__sockets[ip_addr]

            socket.send(msg)
            while (ip_addr not in self.__all_dead_ip_addr):
                try:
                    socket.recv(flags=zmq.NOBLOCK)
                    break
                except zmq.ZMQError, e:
                    if e.errno == zmq.EAGAIN:
                        pass
                    else:
                        raise

    def __notify_all_about_read(self, object_name):
        msg = self.__get_message_format("read", object_name, "yes")

        for ip_addr in self.__all_ip_addr:
            if ip_addr in self.__all_dead_ip_addr:
                continue

            socket = self.__sockets[ip_addr]

            socket.send(msg)
            while (ip_addr not in self.__all_dead_ip_addr):
                try:
                    socket.recv(flags=zmq.NOBLOCK)
                    break
                except zmq.ZMQError, e:
                    if e.errno == zmq.EAGAIN:
                        pass
                    else:
                        raise

    def create_sockets(self):
        for ip_addr in self.__all_ip_addr:
            socket = self.__context.socket(zmq.REQ)
            socket.connect ("tcp://" + ip_addr + ":" + self.__port)
            self.__sockets[ip_addr] = socket

    def init(self, crit_sec_list):
        for crit_sec_name in crit_sec_list:
            self.__crit_sec_map[crit_sec_name] = CriticalSection(crit_sec_name, self.__my_ip_addr)
        self.__thread = Thread(target = self.__handler)
        self.__thread.start()
        time.sleep(3)

    def lock(self, crit_sec_name):
        cs = self.__crit_sec_map[crit_sec_name]
        self.__actual_crit_sec = crit_sec_name
        self.__notify_all_about_locking(crit_sec_name)

    def unlock(self, crit_sec_name):
        cs = self.__crit_sec_map[crit_sec_name]
        self.__notify_all_about_unlocking(crit_sec_name)

    def wait(self, cond_var_name):
        if cond_var_name not in self.__cond_var_map:
            self.__cond_var_map[cond_var_name] = CondVar(cond_var_name)
        self.__notify_all_about_waiting(cond_var_name)
        self.__cond_var_map[cond_var_name].wait()

    def signal(self, cond_var_name):
        self.__notify_all_about_signal(cond_var_name)

    def create_shared_object(self, object_name, object_len):
        if object_name not in self.__object_map:
            self.__object_map[object_name] = Object(object_name, object_len)

    def object_length(self, object_name):
        if object_name in self.__object_map:
            return self.__object_map[object_name].length()
        return None

    def write(self, object_name, object):
        if object_name in self.__object_map:
            ob = self.__object_map[object_name]
            ob.add(object)
            self.__notify_all_about_write(object_name, object)

    def read(self, object_name):
        if object_name in self.__object_map:
            ob = self.__object_map[object_name]
            object = ob.remove()
            self.__notify_all_about_read(object_name)
            return object
        return None
