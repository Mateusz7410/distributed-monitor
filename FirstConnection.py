import zmq
import time
import sys
import socket

class FirstConnection:
    def __init__(self, dst_ip_addr, port, client_count):
        self.__dst_ip_addr = dst_ip_addr
        self.__client_count = client_count - 1
        self.__port = port
        self.__all_ip_addr = []
        self.__token = False
        self.connection()

    def connection(self):
        self.__my_ip_addr = self.__get_local_ip_addr()
        if(self.__my_ip_addr == self.__dst_ip_addr):
            self.__server()
        else:
            self.__client()

    def get_all_ip_addr(self):
        return self.__all_ip_addr

    def get_my_ip_addr(self):
        return self.__my_ip_addr

    def get_token(self):
        return self.__token

    def __get_local_ip_addr(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        res = s.getsockname()[0]
        s.close()
        return res

    def __server(self):
        self.__token = True
        context = zmq.Context()
        socket = context.socket(zmq.REP)
        socket.bind("tcp://*:%s" % self.__port)

        for i in range(self.__client_count):
            message = socket.recv()
            self.__all_ip_addr.append(message)

            socket.send("Connected to server!\nWaiting for other clients...")
        self.__send_to_all()

    def __send_to_all(self):
        msg = ";".join(self.__all_ip_addr)
        msg += ";" + self.__my_ip_addr

        for ip_addr in self.__all_ip_addr:
            context = zmq.Context()
            socket = context.socket(zmq.REQ)
            socket.connect ("tcp://" + ip_addr + ":" + self.__port)
            socket.send (msg)
            message = socket.recv()
        self.__all_ip_addr.append(self.__my_ip_addr)

    def __client(self):
        context = zmq.Context()
        socket = context.socket(zmq.REQ)
        socket.connect ("tcp://" + self.__dst_ip_addr + ":" + self.__port)

        socket.send (self.__my_ip_addr)

        message = socket.recv()

        self.__get_ip_addr()


    def __get_ip_addr(self):
        context = zmq.Context()
        socket = context.socket(zmq.REP)
        socket.bind("tcp://*:%s" % self.__port)

        message = socket.recv()
        socket.send("ok")

        self.__all_ip_addr = message.split(";")
